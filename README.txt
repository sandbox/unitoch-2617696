GSA METATAG
-----------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The GSA Metatag module allow users to select which entity fields will be added
as meta tags on the entity view page, to allow content to be indexed by Google
Search Appliance hardware.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/gsa_metatag


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/gsa_metatag


REQUIREMENTS
------------

This module requires the following modules:

 * Google Search Appliance (https://www.drupal.org/project/google_appliance)
 * Entiy API (https://www.drupal.org/project/entity)


RECOMMENDED MODULES
-------------------

 * GSA Facets (https://drupal.org/project/gsa_facets):


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

 * Customize indexed entity field settings in Administration » Configuration »
   Search and metadataAdministration » Google Appliance » Metatags.


TROUBLESHOOTING
---------------

//TODO


MAINTAINERS
-----------

Current maintainers:

 * David Langarica (morgothz) - https://www.drupal.org/user/1072744/
 * Paul Schulzetenberg (unitoch) - https://www.drupal.org/user/251739

This project has been sponsored by:

 * ORIGIN EIGHT

   Origin Eight is an expert Drupal & open source development agency and
   solutions partner specializing in full-service, strategic, integrated
   solutions for higher education, government, nonprofit organizations and
   private industry. Our top-notch team works collaboratively with clients
   to produce, support and maintain engaging and highly usable web experiences.
